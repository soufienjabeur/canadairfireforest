package canadiar;

import io.jbotsim.core.Color;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;
import io.jbotsim.core.Point;
import io.jbotsim.ui.icons.Icons;

import java.util.ArrayList;
import java.util.List;

public class Sensor extends Node {
	Node parent = null;
	List<Fire> sensedFires = new ArrayList<>();
	int lastReceveidTime;

	@Override
	public void onStart() {
		setIcon(Icons.SENSOR);
		setIconSize(16);
		setCommunicationRange(120);
		setSensingRange(60);
		lastReceveidTime = 0;
	}

	// D�tectation de feu
	@Override
	public void onSensingIn(Node node) {
		// On change la couleur de noeud en cas de feu
		if (node instanceof Fire) {
			this.setColor(Color.red);
			sensedFires.add((Fire) node);
		}
	}

	// Si on a d�tect� y a plus de feu dans une zone
	@Override
	public void onSensingOut(Node node) {
		if (node instanceof Fire) {
			sensedFires.remove((Fire) node);
		}
		if (sensedFires.isEmpty()) {
			this.setColor(null);
		}
	}

	// Si un noeud a re�u le message de construction de l'arbre
	@Override
	public void onMessage(Message message) {
		super.onMessage(message);
		if (parent == null) {
			parent = message.getSender();
			getCommonLinkWith(parent).setWidth(3);
			getCommonLinkWith(parent).setColor(Color.yellow);
			sendAll(new Message());
		}
		if (message.getContent() instanceof Point && message.getSender() instanceof Sensor) {
			// Si il y a un feu au niveau d'une station
			if (sensedFires.isEmpty()) {
				this.setColor(Color.orange);
			}
			// Recepetition d'un message d'alertes
			send(parent, message);
			lastReceveidTime = getTime();
		}
	}

	// Periodiquement, on verifie si la liste de senser fire n'est pas vide
	@Override
	public void onClock() {
		if (!sensedFires.isEmpty()) {
			// On envoie la position de senser pour facilter � la station apr�s le travail
			// de canadair
			send(parent, new Message(this.getLocation()));
		}
		//On doit faire �a periodiquement, 
		if (getTime() - lastReceveidTime >= 10 && this.getColor()==Color.orange) {
			this.setColor(null);
		}
	}
}