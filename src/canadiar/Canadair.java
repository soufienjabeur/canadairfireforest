package canadiar;

import io.jbotsim.core.Message;
import io.jbotsim.core.Node;
import io.jbotsim.core.Point;
import io.jbotsim.ui.icons.Icons;

public class Canadair extends WaypointNode {
	Point parking;
	Point lake = new Point(50, 50);

	public Canadair() {
		setIcon(Icons.CANADAIR);
		setIconSize(16);
		setCommunicationRange(120);
		setSensingRange(30);
	}

	@Override
	public void onStart() {
		parking = getLocation();
		super.setSpeed(10);
	}

	// Si on capte de feu on doit l'eteindre
	@Override
	public void onSensingIn(Node node) {
		if (node instanceof Fire) {
			// Cette methode permet d'eteindre un feu
			((Fire) node).die();
		}
	}

	@Override
	public void onMessage(Message message) {
		super.onMessage(message);
		// S'il s'agit d'un message d'alerte
		if (message.getContent() instanceof Point) {
			Point targetSensor =(Point)message.getContent();
			double r=getSensingRange();
			double x = targetSensor.getX();
			double y = targetSensor.getY();
			Point t1= new Point(x-r, y+r);
			Point t2= new Point(x+r,y+r);
			Point t3= new Point(x+r, y-r);
			Point t4= new Point(x-r, y-r);
			addDestination(lake);
			addDestination(t1);
			addDestination(t2);
			addDestination(t3);
			addDestination(t4);
			addDestination(t1);
			addDestination((Point) message.getContent());
			addDestination(parking);

		}
	}
}