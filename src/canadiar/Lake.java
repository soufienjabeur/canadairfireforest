package canadiar;

import io.jbotsim.core.Node;
import io.jbotsim.ui.icons.Icons;


//Simulation d'eteindre la feu � l'aide de l'eau
public class Lake extends Node {

    public Lake(){
        setIcon(Icons.STATION);
        setIconSize(40);
        disableWireless();
    }
}