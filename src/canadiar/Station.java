package canadiar;

import io.jbotsim.core.Message;
import io.jbotsim.core.Node;
import io.jbotsim.core.Point;
import io.jbotsim.ui.icons.Icons;

public class Station extends Node {

	public Station() {
		setIcon(Icons.STATION);
		setIconSize(25);
		setCommunicationRange(120);
	}

	// Construction de l'arbre couvrant
	@Override
	public void onStart() {
		sendAll(new Message());
	}

	@Override
	public void onMessage(Message message) {
		super.onMessage(message);
		if (message.getContent() instanceof Point) {
			sendAll(message);
		}
	}
}